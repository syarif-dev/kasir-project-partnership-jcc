<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    public function show()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.show', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'jenis_kelamin' => ['required', 'string'],
            'tempat_lahir' => ['required', 'string'],
            'tgl_lahir' => ['required'],
            'alamat' => ['required', 'string'],
            'biodata' => ['required', 'string'],
            'no_telp' => ['required'],
        ]);

        $profile = Profile::find($id);

        if ($request->has('foto')) {
            $path = 'img/img_storage/profile/';
            \Illuminate\Support\Facades\File::delete($path . $profile->foto);
            $gambar = $request['foto'];
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);


            $profile_data = [
                'umur' => $request['umur'],
                'jenis_kelamin' => $request['jenis_kelamin'],
                'tempat_lahir' => $request['tempat_lahir'],
                'tgl_lahir' => $request['tgl_lahir'],
                'alamat' => $request['alamat'],
                'biodata' => $request['biodata'],
                'no_telp' => $request['no_telp'],
                'foto' => $new_gambar,
            ];
        } else {
            $profile_data = [
                'umur' => $request['umur'],
                'jenis_kelamin' => $request['jenis_kelamin'],
                'tempat_lahir' => $request['tempat_lahir'],
                'tgl_lahir' => $request['tgl_lahir'],
                'alamat' => $request['alamat'],
                'biodata' => $request['biodata'],
                'no_telp' => $request['no_telp']
            ];
        }

        Profile::whereId($id)->update($profile_data);
        Alert::success('Berhasil', 'Mengubah Profile');
        return redirect('/profile');
    }
}
