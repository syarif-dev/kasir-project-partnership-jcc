<?php

namespace App\Http\Controllers;

use PDF;
use Alert;
use App\Models\User;
use App\Exports\UserExport;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use App\Exports\TransaksiPembelianBarangExport;


class TransaksiPembelianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::all();
        return view('transaksi_pembelian_barang.index', compact('transaksiPembelianBarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masterBarang = MasterBarang::all();
        $transaksiPembelianBarang = TransaksiPembelianBarang::all();
        $transaksiPembelian = TransaksiPembelian::all();
        return view('transaksi_pembelian_barang.create', compact('masterBarang','transaksiPembelianBarang','transaksiPembelian'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'transaksi_pembelian_id' => 'required',
            'master_barang_id' => 'required',
            'jumlah' => 'required',
            'harga_satuan' => 'required',
        ]);

        if ($request->transaksi_pembelian_id == 0) {

            $transaksi = TransaksiPembelian::create([
                "total_harga" => $request["jumlah"] * $request["harga_satuan"],
            ]);

            TransaksiPembelianBarang::create([
                "transaksi_pembelian_id" => $transaksi->id,
                "master_barang_id" => $request->master_barang_id,
                "jumlah" => $request->jumlah,
                "harga_satuan" => $request->harga_satuan
            ]);

            Alert::success('Berhasil', 'Transasksi Pembelian Sukses');
            return redirect('/transaksi-pembelian-barang');
        }


        // $findTransaksi = TransaksiPembelian::find($request["transaksi_pembelian_id"]);
        // $totalHarga = $request["jumlah"] * $request["harga_satuan"];
        // $seluruhTotalHarga = ($findTransaksi['total_harga'] + $totalHarga);

        TransaksiPembelianBarang::create([
            "transaksi_pembelian_id" => $request->transaksi_pembelian_id,
            "master_barang_id" => $request->master_barang_id,
            "jumlah" => $request->jumlah,
            "harga_satuan" => $request->harga_satuan
        ]);

        $findTransaksi = TransaksiPembelian::find($request->transaksi_pembelian_id);
        $totalHarga = $findTransaksi['total_harga'] + ($request->harga_satuan * $request->jumlah);
        TransaksiPembelian::where('id', $request->transaksi_pembelian_id)->update([
            'total_harga' => $totalHarga
        ]);
        // $dataBaru = [
        //     "total_harga" => $seluruhTotalHarga,
        // ];
        // $findTransaksi->update($dataBaru);


        // $total_harga = $request["jumlah"] * $request["harga_satuan"];

        // if ($data["transaksi_pembelian_id"] == 0) {
        //     $transaksiPembelian = TransaksiPembelian::create([
        //         "total_harga" => $total_harga,
        //     ]);

        //     TransaksiPembelianBarang::create([
        //         "transaksi_pembelian_id" => $transaksiPembelian->id,
        //         "master_barang_id" => $data["master_barang_id"],
        //         "jumlah" => $data["jumlah"],
        //         "harga_satuan" => $data["harga_satuan"]
        //     ]);
        //     Alert::success('Berhasil', 'Transasksi Pembelian Sukses');
        //     return redirect('/transaksi-pembelian-barang');
        // }

        // $findTransaksi = TransaksiPembelian::find($request["transaksi_pembelian_id"]);
        // $totalHarga = $request["jumlah"] * $request["harga_satuan"];
        // $seluruhTotalHarga = ($findTransaksi['total_harga'] + $totalHarga);


        // $dataBaru = [
        //     "total_harga" => $seluruhTotalHarga,
        // ];
        // $findTransaksi->update($dataBaru);

        // TransaksiPembelianBarang::create([
        //     "transaksi_pembelian_id" => $request["transaksi_pembelian_id"],
        //     "master_barang_id" => $data["master_barang_id"],
        //     "jumlah" => $data["jumlah"],
        //     "harga_satuan" => $data["harga_satuan"]
        // ]);
        Alert::success('Berhasil', 'Transaksi Sukses');
        return redirect('/transaksi-pembelian-barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::find($id);

        return view('transaksi_pembelian_barang.show', compact('transaksiPembelianBarang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::find($id);
        $transaksiPembelian = TransaksiPembelian::all();
        $masterBarang = MasterBarang::all();
        return view('transaksi_pembelian_barang.edit', compact('transaksiPembelianBarang', 'transaksiPembelian', 'masterBarang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $findTransaksi = TransaksiPembelianBarang::find($id);
        $hargaLama = $findTransaksi['harga_satuan'] * $findTransaksi['jumlah'];

        $this->validate($request,[
            'transaksi_pembelian_id' => 'required',
            'master_barang_id' => 'required',
            'jumlah' => 'required',
            'harga_satuan' => 'required',
        ]);

        TransaksiPembelianBarang::where('id',$id)->update([
            'transaksi_pembelian_id' => $request->transaksi_pembelian_id,
            'master_barang_id' => $request->master_barang_id,
            'jumlah' => $request-> jumlah,
            'harga_satuan' => $request->harga_satuan,
        ]);
        $hargaBaru = $request->harga_satuan * $request->jumlah;
        $totalHarga = TransaksiPembelian::find($request->transaksi_pembelian_id);
        $updateTotal = ($totalHarga['total_harga']+ $hargaBaru - $hargaLama);

        TransaksiPembelian::where('id', $request->transaksi_pembelian_id)->update([
            'total_harga' => $updateTotal
        ]);

        Alert::success('Berhasil', 'Transaksi Berhasil Diperbarui');
        return redirect('/transaksi-pembelian-barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::find($id);
        $transaksipembelian = TransaksiPembelian::find($transaksiPembelianBarang->transaksi_pembelian_id);

        $total_harga_lama = $transaksiPembelianBarang["jumlah"] * $transaksiPembelianBarang["harga_satuan"];
        $seluruh_total_harga = ($transaksipembelian['total_harga'] - $total_harga_lama);
        $transaksi_pembelian = [
            "total_harga" => $seluruh_total_harga,

        ];
        $transaksipembelian->update($transaksi_pembelian);
        $transaksiPembelianBarang->delete();
        Alert::success('Berhasil', 'Menghapus Data Transaksi Pembelian Barang');
        return redirect('/transaksi-pembelian-barang');
    }

    public function getDetails($id = 0)
    {
        $data = MasterBarang::where('id',$id)->first();
        return response()->json($data);
    }

    public function pdf()
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::all();
        $pdf = PDF::loadview('transaksi_pembelian_barang.pdf', compact('transaksiPembelianBarang'));
        return $pdf->stream('transaksi_pembelian_barang.pdf');
    }

    public function print()
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::all();
        return view('transaksi_pembelian_barang.print', compact('transaksiPembelianBarang'));
    }

    public function pdf_detail($id)
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::find($id);
        $pdf = PDF::loadview('transaksi_pembelian_barang.pdf_detail', compact('transaksiPembelianBarang'));
        return $pdf->stream('transaksi_pembelian_barang_detail.pdf');
    }

    public function print_detail($id)
    {
        $transaksiPembelianBarang = TransaksiPembelianBarang::find($id);
        return view('transaksi_pembelian_barang.print_detail', compact('transaksiPembelianBarang'));
    }

    public function excel()
    {
        return \Maatwebsite\Excel\Facades\Excel::download(new TransaksiPembelianBarangExport, 'transaksi-pembelian-barang.xlsx');
    }

}
