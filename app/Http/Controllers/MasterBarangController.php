<?php

namespace App\Http\Controllers;

use PDF;
use Alert;
use App\Exports\UserExport;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Exports\MasterBarangExport;


class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masterBarang = MasterBarang::all();
        return view('master_barang.index', compact('masterBarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master_barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_barang' => 'required',
            'harga_satuan' => 'required'
        ],[
            "nama_barang.required" => "Nama barang harus diisi",
            "harga_satuan.required" => "Harga satuan harus diisi",
        ]);

        MasterBarang::create([
            "nama_barang" => $request["nama_barang"],
            "harga_satuan" => $request["harga_satuan"]
        ]);

        Alert::success('success','Data Barang baru berhasil disimpan');
        return redirect('/master-barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterBarang = MasterBarang::find($id);
        return view('master_barang.show', compact('masterBarang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterBarang = MasterBarang::find($id);
        return view('master_barang.edit', compact('masterBarang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "nama_barang" => 'required',
            "harga_satuan" => 'required'
        ],[
            "nama_barang.required" => "Nama barang harus diisi",
            "harga_satuan.required" => "harga satuan harus diisi"
        ]);

        MasterBarang::where('id',$id)->update([
            "nama_barang" => $request->nama_barang,
            "harga_satuan" => $request->harga_satuan
        ]);

        Alert::success('success','Data Barang berhasil diperbarui');
        return redirect('/master-barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterBarang = MasterBarang::find($id);
        $masterBarang->delete();
        Alert::success('success','Data Barang berhasil dihapus');
        return redirect('/master-barang');
    }

    public function pdf()
    {
        $masterBarang = MasterBarang::all();
        $pdf = PDF::loadview('master_barang.pdf', compact('masterBarang'));
        return $pdf->stream('master_barang.pdf');
    }

    public function print()
    {
        $masterBarang = MasterBarang::all();
        return view('master_barang.print', compact('masterBarang'));
    }

    public function pdf_detail($id)
    {
        $masterBarang = MasterBarang::find($id);
        $pdf = PDF::loadview('master_barang.pdf_detail', compact('masterBarang'));
        return $pdf->stream('master_barang_detail.pdf');
    }

    public function print_detail($id)
    {
        $masterBarang = MasterBarang::find($id);
        return view('master_barang.print_detail', compact('masterBarang'));
    }

    public function excel()
    {
        return \Maatwebsite\Excel\Facades\Excel::download(new MasterBarangExport, 'master-barang.xlsx');
    }
}
