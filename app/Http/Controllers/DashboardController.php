<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Profile;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;

class DashboardController extends Controller
{
    public function index(){

        $user = User::count();
        $profile = Profile::count();
        $masterBarang = MasterBarang::count();
        $transaksiPembelian = TransaksiPembelian::count();
        $transaksiPembelianBarang = TransaksiPembelianBarang::count();

        return view('dashboard', compact('user', 'profile', 'masterBarang', 'transaksiPembelian', 'transaksiPembelianBarang'));
    }
}
