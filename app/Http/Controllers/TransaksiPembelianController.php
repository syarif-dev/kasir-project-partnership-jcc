<?php

namespace App\Http\Controllers;

use PDF;
use App\Exports\UserExport;
use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Exports\TransaksiPembelianExport;


class TransaksiPembelianController extends Controller
{
    public function index(){
        $transaksiPembelian = TransaksiPembelian::all();
        return view('transaksi_pembelian.index', compact('transaksiPembelian'));
    }

    public function show($id){
        $transaksiPembelian = TransaksiPembelian::find($id);
        return view('transaksi_pembelian.show', compact('transaksiPembelian'));
    }

    public function pdf()
    {
        $transaksiPembelian = TransaksiPembelian::all();
        $pdf = PDF::loadview('transaksi_pembelian.pdf', compact('transaksiPembelian'));
        return $pdf->stream('transaksi_pembelian.pdf');
    }

    public function print()
    {
        $transaksiPembelian = TransaksiPembelian::all();
        return view('transaksi_pembelian.print', compact('transaksiPembelian'));
    }

    public function pdf_detail($id)
    {
        $transaksiPembelian = TransaksiPembelian::find($id);
        $pdf = PDF::loadview('transaksi_pembelian.pdf_detail', compact('transaksiPembelian'));
        return $pdf->stream('transaksi_pembelian_detail.pdf');
    }

    public function print_detail($id)
    {
        $transaksiPembelian = TransaksiPembelian::find($id);
        return view('transaksi_pembelian.print_detail', compact('transaksiPembelian'));
    }

    public function excel()
    {
        return \Maatwebsite\Excel\Facades\Excel::download(new TransaksiPembelianExport, 'transaksi-pembelian.xlsx');
    }
}
