<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profile';
    protected $fillable = ["umur", "jenis_kelamin", "tempat_lahir", "tgl_lahir", "alamat", "biodata", "no_telp", "foto", "user_id"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
