<?php

namespace App\Exports;

use App\Models\TransaksiPembelianBarang;
use Maatwebsite\Excel\Concerns\FromCollection;

class TransaksiPembelianBarangExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return TransaksiPembelianBarang::all();
    }
}
