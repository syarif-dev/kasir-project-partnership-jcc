<?php

namespace App\Exports;

use App\Models\TransaksiPembelian;
use Maatwebsite\Excel\Concerns\FromCollection;

class TransaksiPembelianExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return TransaksiPembelian::all();
    }
}
