<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = '12345678';
        User::create([
            'name' => 'syarif hidayatulloh',
            'username' => 'syarif',
            'email' => 'syarif@gmail.com',
            'password' => Hash::make($password),
            'role' => 'Admin',
        ]);

        User::create([
            'name' => 'budi setya',
            'username' => 'budi',
            'email' => 'budi@gmail.com',
            'password' => Hash::make($password),
            'role' => 'Kasir',
        ]);

        User::create([
            'name' => 'candra gunawan',
            'username' => 'candra',
            'email' => 'candra@gmail.com',
            'password' => Hash::make($password),
            'role' => 'Kasir',
        ]);

        Profile::create([
            'umur' => 23,
            'jenis_kelamin' => 'Laki-laki',
            'tempat_lahir' => 'sidoarjo',
            'tgl_lahir' => '1999-05-22',
            'alamat' => 'surabaya',
            'biodata' => 'I like programming',
            'no_telp' => '082111111',
            'foto' => 'default.svg',
            'user_id' => 1,
        ]);

        Profile::create([
            'umur' => 23,
            'jenis_kelamin' => 'Laki-laki',
            'tempat_lahir' => 'sidoarjo',
            'tgl_lahir' => '1999-05-22',
            'alamat' => 'surabaya',
            'biodata' => 'I like programming',
            'no_telp' => '082111111',
            'foto' => 'default.svg',
            'user_id' => 2,
        ]);

        Profile::create([
            'umur' => 23,
            'jenis_kelamin' => 'Laki-laki',
            'tempat_lahir' => 'sidoarjo',
            'tgl_lahir' => '1999-05-22',
            'alamat' => 'surabaya',
            'biodata' => 'I like programming',
            'no_telp' => '082111111',
            'foto' => 'default.svg',
            'user_id' => 3,
        ]);
    }
}
