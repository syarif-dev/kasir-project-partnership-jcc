@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Transaksi Pembelian Barang
@endsection
@section('subtitle')
Total Transaksi Pembelian Barang
@endsection
@section('content')
<a href="{{ url('transaksi-pembelian-barang/create') }}"><button type="button" class="btn btn-primary"><i
            class="fas fa-plus-square"></i></button></a>
<a href="{{ url('pdf-transaksi-pembelian-barang') }}"><button type="button" class="btn btn-danger"><i
            class="fas fa-file-pdf"></i></button></a>
<a href="{{ url('print-transaksi-pembelian-barang') }}"><button type="button" class="btn btn-warning"><i
            class="fas fa-print"></i></button></a>
<a href="{{ url('excel-transaksi-pembelian-barang') }}"><button type="button" class="btn btn-success"><i
            class="fas fa-file-excel"></i></button></a>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Total Transaksi Pembelian Barang</h6>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Transaksi</th>
                        <th>Total Harga Transaksi</th>
                        <th>Waktu Transaksi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($transaksiPembelian as $key => $item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->total_harga }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td class="text-center">
                            <a href="/transaksi-pembelian/{{$item->id}}" class="btn btn-info my-1"><i
                                    class="fas fa-eye"></i></a>
                        </td>
                    </tr>
                    @empty
                    <h1>Tidak ada data transaksi pembelian barang</h1>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nomor Transaksi</th>
                        <th>Total Harga Transaksi</th>
                        <th>Waktu Transaksi</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.js"></script>
@endpush
