@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Total Transaksi
@endsection
@section('subtitle')
Detail Data Akhir Transaksi
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
Detail Data Akhir Transaksi
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>

    <div class="card-body">
        <div class="mb-3">
            <a href="{{ url('pdf-transaksi-pembelian-detail') }}/{{ $transaksiPembelian->id }}"><button type="button"
                    class="btn btn-danger"><i class="fas fa-file-pdf"></i></button></a>
            <a href="{{ url('print-transaksi-pembelian-detail')}}/{{ $transaksiPembelian->id }}"><button type="button"
                    class="btn btn-warning"><i class="fas fa-print"></i></button></a>
        </div>
        <h5 class="text-right">Waktu Transaksi : {{ $transaksiPembelian->created_at }}</h5>
        <h3 class="text-right"><b>Total Harga : </b>{{ $transaksiPembelian->total_harga }}</h3>
        <div class="table-responsive">
            <table id="example1" class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Harga Satuan</th>
                        <th>Jumlah</th>
                        <th>Sub Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaksiPembelian->transaksi_pembelian_barang as $key => $item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->master_barang->nama_barang }}</td>
                        <td>{{ $item->harga_satuan }}</td>
                        <td>{{ $item->jumlah }}</td>
                        <td class="text-right">Rp {{ $item->harga_satuan * $item->jumlah}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Harga Satuan</th>
                        <th>Jumlah</th>
                        <th>Sub Total</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        {{-- @foreach ($transaksiPembelian->transaksi_pembelian_barang as $key => $item)
        <h4 class="card-text"><b>Transaksi Pembelian {{ $key + 1  }}</b> : </h4>
        <ul>
            <li>
                <h4 class="card-text"><b>Nama Barang</b> : {{ $item->master_barang->nama_barang }}</h4>
            </li>
            <li>
                <h4 class="card-text"><b>Harga Satuan</b> : {{ $item->harga_satuan }}</h4>
            </li>
            <li>
                <h4 class="card-text"><b>Jumlah Barang</b> : {{ $item->jumlah }}</h4>
            </li>
            <li>
                <h4 class="card-text"><b>Jumlah Barang</b> : {{ $item->harga_satuan * $item->jumlah}}</h4>
            </li>
        </ul>
        @endforeach --}}
        
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.js"></script>
@endpush