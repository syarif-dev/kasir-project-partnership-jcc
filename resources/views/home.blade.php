@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Home
@endsection
@section('subtitle')
Home
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Home') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}

                    <div class="h2">Project 2 - Aplikasi Kasir (Laravel Project)</div>
                    <hr style="width:50%"> Jabar Coding Camp (JCC), Aplikasi Kasir (Laravel Project), Anda diminta untuk membuat
                    aplikasi
                    kasir sederhana Kelas JCC Partnership - Project Challenge (Tantangan).<br><br>
                    <div class="h4"> diminta untuk membuat aplikasi kasir sederhana yang mencakup fitur: </div>
                    <ul>
                        <li>Mencatat transaksi pembelian barang</li>
                        <li>Menampilkan daftar transaksi pembelian barang</li>
                        <li>Authentikasi login pengguna</li>
                        <li>Pengaturan data pengguna</li>
                        <li>Pengaturan data master produk</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
