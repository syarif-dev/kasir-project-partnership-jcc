@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Profile
@endsection
@section('subtitle')
Profile
@endsection
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Profile</h6>
    </div>

    <div class="card-body text-center">
        <img class="img-profile rounded-circle mb-3"
            src="{{ asset('/img') }}/img_storage/profile/{{ $profile->foto }}" width="300px" height="300px"
            alt="User profile picture">
            {{-- <img class="img-profile rounded-circle mb-3"
            src="{{ asset('/img') }}/img_storage/profile/{{ $profile->foto }}" width="300px" height="300px"
            alt="User profile picture"> --}}
        <h4 class="card-text"><b>Nama Profile</b> : {{ $profile->user->name }}</h4>
        <h4 class="card-text"><b>Role</b> : {{ $profile->user->role }}</h4>
        <hr style="width:75%">
        <h5 class="card-text text-left"><b>Nama Panggilan</b> : {{ $profile->user->username }}</h5>
        <h5 class="card-text text-left"><b>Email</b> : {{ $profile->user->email }}</h5>
        <h5 class="card-text text-left"><b>Umur</b> : {{ $profile->umur }}</h5>
        <h5 class="card-text text-left"><b>Tempat dan Tanggal Lahir</b> : {{ $profile->tempat_lahir }},
            {{ $profile->tgl_lahir }}</h5>
        <h5 class="card-text text-left"><b>Jenis Kelamin</b> : {{ $profile->jenis_kelamin }}</h5>
        <h5 class="card-text text-left"><b>Bio</b> : {!! $profile->biodata !!}</h5>
        <h5 class="card-text text-left"><b>Alamat</b> : {!! $profile->alamat !!}</h5>
        <h5 class="card-text text-left"><b>Nomor Telepon</b> : {!! $profile->no_telp !!}</h5>
        <h5 class="card-text text-left"><b>Akun Dibuat</b> : {{ $profile->user->created_at }}</h5>
        <h5 class="card-text text-left"><b>Akun Diupdate</b> : {{ $profile->user->updated_at }}</h5>
        <hr style="width:75%">
        <a href="{{ url('profile/show') }}" class="btn btn-outline-dark">Ubah Profile</a>
    </div>
</div>
@endsection

