@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Profile
@endsection
@section('subtitle')
Profile
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Profile</h6>
    </div>
    <div class="card-body">
        <form action="/profile/{{ $profile->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" email="email" value="{{$profile->user->email}}" id="email"
                    placeholder="Masukkan email" disabled>
                @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="name">Nama Lengkap</label>
                <input type="text" class="form-control" name="name" value="{{$profile->user->name}}" id="name"
                    placeholder="Masukkan name" disabled>
                @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="username">Nama Panggilan</label>
                <input type="text" class="form-control" name="username" value="{{$profile->user->username}}" id="username"
                    placeholder="Masukkan Nama Panggilan" disabled>
                @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                <input type="text" class="form-control" name="role" value="{{$profile->user->role}}" id="role"
                    placeholder="Masukkan Role" disabled>
                @error('role')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur"
                    value="{{ $profile->umur }}">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="jenis_kelamin" class="col-form-label text-md-right">{{ __('Jenis Kelamin') }}</label>
                <div class="col-md-6">
                    <div class="form-check">
                        @if ($profile->jenis_kelamin )
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios1" value="Laki-laki"
                            checked>
                        @else
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios1" value="Laki-laki">
                        @endif
                        <label class="form-check-label" for="exampleRadios1">
                            Laki-laki
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios2" value="Perempuan">
                        <label class="form-check-label" for="exampleRadios2">
                            Perempuan
                        </label>
                    </div>
                    @error('jenis_kelamin')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir"
                    placeholder="Masukkan Tempat Lahir" value="{{ $profile->tempat_lahir }}">
                @error('tempat_lahir')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="Masukkan Tanggal Lahir"
                    value="{{ $profile->tgl_lahir }}">
                @error('tgl_lahir')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="biodata">Bio</label>
                <textarea class="form-control" name="biodata" id="biodata" rows="3"
                    placeholder="Masukkan Bio">{{$profile->biodata}}</textarea>
                @error('biodata')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" name="alamat" id="alamat" rows="3"
                    placeholder="Masukkan Alamat">{{$profile->alamat}}</textarea>

                @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="no_telp">Nomor Telepon</label>
                <input type="number" class="form-control" name="no_telp" id="no_telp" placeholder="Masukkan Nomor Telepon"
                    value="{{ $profile->no_telp }}">
                @error('no_telp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="foto" class="text-md-right">{{ __('Profile Foto') }}</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="foto">
                @error('foto')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
            <a href="{{ url('profile') }}" class="btn btn-danger">Kembali</a>
        </form>
    </div>
</div>
@endsection
