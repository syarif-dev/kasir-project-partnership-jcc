@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Master Barang
@endsection
@section('subtitle')
Detail Data Barang
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail Data Barang</h6>
    </div>

    <div class="card-body">
        <h4 class="card-text"><b>ID Barang</b> : {{ $masterBarang->id }}</h4>
        <h4 class="card-text"><b>Nama Barang</b> : {{ $masterBarang->nama_barang }}</h4>
        <h4 class="card-text"><b>Harga Satuan</b> : {{ $masterBarang->harga_satuan }}</h4>
    </div>
</div>

{{-- @push('scripts')
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
        toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
   });
</script>
@endpush --}}
@endsection
