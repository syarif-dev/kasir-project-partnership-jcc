<h1 class="h3 mb-2 text-gray-800">@yield('subtitle')</h1>
@yield('content')
