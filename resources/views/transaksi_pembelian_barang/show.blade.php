@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Transaksi Pembelian Barang
@endsection
@section('subtitle')
Detail Data Transaksi
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail Data Transaksi</h6>
    </div>

    <div class="card-body">
        <div class="mb-3">
            <a href="{{ url('pdf-transaksi-pembelian-barang-detail') }}/{{ $transaksiPembelianBarang->id }}"><button type="button"
                    class="btn btn-danger"><i class="fas fa-file-pdf"></i></button></a>
            <a href="{{ url('print-transaksi-pembelian-barang-detail')}}/{{ $transaksiPembelianBarang->id }}"><button type="button"
                    class="btn btn-warning"><i class="fas fa-print"></i></button></a>
        </div>
        <h4 class="card-text"><b>Nomor Daftar Transaksi Pembelian</b> : {{ $transaksiPembelianBarang->transaksi_pembelian_id }}</h4>
        <h4 class="card-text"><b>Nama Barang</b> : {{ $transaksiPembelianBarang->master_barang->nama_barang }}</h4>
        <h4 class="card-text"><b>Jumlah</b> : {{ $transaksiPembelianBarang->jumlah }}</h4>
        <h4 class="card-text"><b>Harga Satuan</b> : {{ $transaksiPembelianBarang->harga_satuan }}</h4>
        <h4 class="card-text"><b>Harga Total</b> : {{ $transaksiPembelianBarang->harga_satuan * $transaksiPembelianBarang->jumlah }}</h4>
        <h4 class="card-text"><b>Harga Dibuat</b> : {{ $transaksiPembelianBarang->created_at }}</h4>
        <h4 class="card-text"><b>Waktu Diupdate</b> : {{ $transaksiPembelianBarang->updated_at }}</h4>
    </div>
</div>

{{-- @push('scripts')
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
        toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
   });
</script>
@endpush --}}
@endsection
