@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('title')
Aplikasi Kasir | Transaksi Pembelian Barang
@endsection
@section('subtitle')
Tambah Transaksi Pembelian Barang
@endsection
@section('content')
<div>
    <a href="{{ url('transaksi-pembelian-barang') }}" class="btn btn-danger my-2">Kembali</a>
</div>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Transaksi Pembelian Barang</h6>
    </div>

    <div class="card-body">
        <form action="/transaksi-pembelian-barang" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="transaksi_pembelian_id">Nomor Transaksi</label>
                    <select class="js-example-basic-single form-control" name="transaksi_pembelian_id" id="transaksi_pembelian_id">
                        <option>Pilih Nomor Transaksi</option>
                        <option value="0">Transaksi Baru</option>
                        @foreach ($transaksiPembelian as $item)
                            <option value="{{$item->id}}">{{$item->id}}</option>
                        @endforeach
                    </select>
                    @error('transaksi_pembelian_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="master_barang_id">Nama Barang</label>
                    <select class="js-example-basic-single form-control" name="master_barang_id" id="master_barang_id">
                        <option value="0">Pilih Barang</option>
                        @forelse ($masterBarang as $key => $item)
                            <option value="{{$item->id}}">{{$item->nama_barang}}</option>
                        @empty
                            <option value="">--Tidak ada Data Barang--</option>
                        @endforelse
                    </select>
                    @error('master_barang_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    {{-- <a href="javascript:;" id="getData" >Get Data</a> --}}
                    <label for="harga_satuan">Harga Satuan</label>
                    <input type="number" class="form-control" name="harga_satuan" id="harga_satuan" readonly>
                    @error('harga_satuan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" min="0" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan Jumlah Barang">
                    @error('jumlah')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label for="subtotal">Subtotal</label>
                    <input type="number" class="form-control" name="subtotal" id="subtotal" disabled>
                    @error('subtotal')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="transaksi">
            </div>
            <button type="submit" class="btn btn-primary" style="float: right">Simpan</button>
        </form>
    </div>
</div>
@endsection

@push('scripts')
{{-- <script
    src=//code.jquery.com/jquery-3.5.1.min.js
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin=anonymous></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
<script type="text/javascript">

    $('#master_barang_id').change(function() {
        var idformat = $(this).val();
        var url = '{{route("getDetails", ":id") }}';
        url = url.replace(':id', idformat);

        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            success: function(response){
                if (response != null) {
                    $('#harga_satuan').val(response.harga_satuan);
                }
            }
        });

        });
</script>
<script>
    $("#jumlah").change(function(){
        var jumlah = $(this).val()
        var harga = $("#harga_satuan").val();
        var subTotal = jumlah*harga;
        $('#subtotal').val(subTotal);
    });
</script>
{{-- <script>
    $(".btnTransaksi").click(function(){
        addTransaksi()
    });
    function addTransaksi(){
        var transaksi =`<div class="row">
                <div class="form-group col-md-3">
                    <label for="nama_barang[]">Nama Barang</label>
                    <select class="js-example-basic-single form-control" name="nama_barang[]" id="nama_barang[]">
                        <option value="0">Pilih Barang</option>
                        @forelse ($masterBarang as $key => $item)
                            <option value="{{$item->id}}">{{$item->nama_barang}}</option>
                        @empty
                            <option value="">--Tidak ada Data Barang--</option>
                        @endforelse
                    </select>
                    @error('nama_barang[]')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <a href="javascript:;" id="getData" >Get Data</a>
                    <label for="harga_satuan[]">Harga Satuan</label>
                    <input type="number" class="form-control" name="harga_satuan[]" id="harga_satuan[]" disabled>
                    @error('harga_satuan[]')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label for="jumlah[]">Jumlah</label>
                    <input type="number" min="0" class="form-control" name="jumlah[]" id="jumlah[]" placeholder="Masukkan Jumlah Barang">
                    @error('jumlah[]')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group col-md-2">
                    <label for="subtotal[]">Subtotal</label>
                    <input type="number" class="form-control" name="subtotal[]" id="subtotal[]" disabled>
                    @error('subtotal[]')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div>
                <a href="#"><button type="button" class="remove btn btn-danger mt-4"><i
            class="fas fa-minus-square"></i></button></a></div>
            </div>`;
        $(".transaksi").append(transaksi);
    }

    $( document ).on( "click", ".remove", function() {
        $(this).parent().parent().parent().remove();
    });
</script> --}}

@endpush

