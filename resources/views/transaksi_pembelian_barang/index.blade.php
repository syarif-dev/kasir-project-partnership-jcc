@extends('template.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
Aplikasi Kasir | Transaksi Pembelian Barang
@endsection
@section('subtitle')
Transaksi Pembelian Barang
@endsection
@section('content')
<a href="{{ url('transaksi-pembelian/create') }}"><button type="button" class="btn btn-primary"><i
            class="fas fa-plus-square"></i></button></a>
<a href="{{ url('pdf-transaksi-pembelian') }}"><button type="button" class="btn btn-danger"><i
            class="fas fa-file-pdf"></i></button></a>
<a href="{{ url('print-transaksi-pembelian') }}"><button type="button" class="btn btn-warning"><i
            class="fas fa-print"></i></button></a>
<a href="{{ url('excel-transaksi-pembelian') }}"><button type="button" class="btn btn-success"><i
            class="fas fa-file-excel"></i></button></a>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Transaksi Pembelian Barang</h6>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example1" class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Transaksi Pembelian Id</th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Harga Satuan</th>
                        <th>Total Harga</th>
                        <th>Waktu Dibuat</th>
                        <th>Waktu Diupdate</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($transaksiPembelianBarang as $key => $item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->transaksi_pembelian_id }}</td>
                        <td>{{ $item->master_barang->nama_barang }}</td>
                        <td>{{ $item->jumlah }}</td>
                        <td>{{ $item->harga_satuan }}</td>
                        <td>{{ $item->harga_satuan * $item->jumlah }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td>{{ $item->updated_at }}</td>
                        <td class="text-center">
                            <form action="/transaksi-pembelian-barang/{{$item->id}}" method="POST" class="display-non">
                                <a href="/transaksi-pembelian-barang/{{$item->id}}" class="btn btn-info my-1"><i
                                    class="fas fa-eye"></i></a>
                                {{-- @auth --}}
                                <a href="/transaksi-pembelian-barang/{{$item->id}}/edit" class="btn btn-primary my-1"><i
                                        class="far fa-edit"></i></a>
                                @csrf
                                @method('DELETE')
                                <button input type="submit" class="btn btn-danger my-1" value="Delete"><i
                                        class="far fa-trash-alt"></i></button>
                            </form>
                            {{-- @endauth --}}
                        </td>
                    </tr>
                    @empty
                    <h1>Tidak ada data transaksi pembelian barang</h1>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Transaksi Pembelian Id</th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Harga Satuan</th>
                        <th>Total Harga</th>
                        <th>Waktu Dibuat</th>
                        <th>Waktu Diupdate</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.js"></script>
@endpush
@endsection
