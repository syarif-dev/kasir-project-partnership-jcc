<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\TransaksiPembelianController;
use App\Http\Controllers\TransaksiPembelianBarangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function(){
    Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('checkRole:Admin,Kasir');
    Route::resource('/dashboard', DashboardController::class)->only(['index'])->middleware('checkRole:Admin');

    Route::resource('/master-barang', MasterBarangController::class)->middleware('checkRole:Admin');
    Route::resource('/transaksi-pembelian-barang', TransaksiPembelianBarangController::class)->middleware('checkRole:Admin,Kasir');
    Route::resource('/transaksi-pembelian', TransaksiPembelianController::class)->only(['index', 'show'])->middleware('checkRole:Admin,Kasir');
    Route::resource('/profile', ProfileController::class)->only(['index', 'update', 'show'])->middleware('checkRole:Admin,Kasir');
    Route::resource('/user', UserController::class)->middleware('checkRole:Admin');

    Route::get('/get/details/{id}', [TransaksiPembelianBarangController::class,'getDetails'])->name('getDetails')->middleware('checkRole:Admin,Kasir');

    // DATA PDF
    Route::get('/pdf-transaksi-pembelian', [TransaksiPembelianController::class, 'pdf'])->name('pdf-transaksi-pembelian')->middleware('checkRole:Admin,Kasir');
    Route::get('/pdf-transaksi-pembelian-barang', [TransaksiPembelianbarangController::class, 'pdf'])->name('pdf-transaksi-pembelian-barang')->middleware('checkRole:Admin,Kasir');
    Route::get('/pdf-master-barang', [MasterbarangController::class, 'pdf'])->name('pdf-master-barang')->middleware('checkRole:Admin');
    Route::get('/pdf-user', [UserController::class, 'pdf'])->name('pdf-user')->middleware('checkRole:Admin');

    // DATA EXCEL
    Route::get('/excel-transaksi-pembelian', [TransaksiPembelianController::class, 'excel'])->name('excel-transaksi-pembelian')->middleware('checkRole:Admin,Kasir');
    Route::get('/excel-transaksi-pembelian-barang', [TransaksiPembelianbarangController::class, 'excel'])->name('excel-transaksi-pembelian-barang')->middleware('checkRole:Admin,Kasir');
    Route::get('/excel-master-barang', [MasterbarangController::class, 'excel'])->name('excel-master-barang')->middleware('checkRole:Admin');
    Route::get('/excel-user', [UserController::class, 'excel'])->name('excel-user')->middleware('checkRole:Admin');

    // DATA Print
    Route::get('/print-transaksi-pembelian', [TransaksiPembelianController::class, 'print'])->name('print-transaksi-pembelian')->middleware('checkRole:Admin,Kasir');
    Route::get('/print-transaksi-pembelian-barang', [TransaksiPembelianbarangController::class, 'print'])->name('print-transaksi-pembelian-barang')->middleware('checkRole:Admin,Kasir');
    Route::get('/print-master-barang', [MasterbarangController::class, 'print'])->name('print-master-barang')->middleware('checkRole:Admin,Kasir');
    Route::get('/print-user', [UserController::class, 'print'])->name('print-user')->middleware('checkRole:Admin,Kasir');

    // DATA PDF Detail
    Route::get('/pdf-transaksi-pembelian-detail/{id}', [TransaksiPembelianController::class, 'pdf_detail'])->name('pdf-transaksi-pembelian-detail')->middleware('checkRole:Admin,Kasir');
    Route::get('/pdf-transaksi-pembelian-barang-detail/{id}', [TransaksiPembelianbarangController::class, 'pdf_detail'])->name('pdf-transaksi-pembelian-barang-detail')->middleware('checkRole:Admin,Kasir');
    Route::get('/pdf-master-barang-detail/{id}', [MasterbarangController::class, 'pdf_detail'])->name('pdf-master-barang-detail')->middleware('checkRole:Admin');
    Route::get('/pdf-user-detail/{id}', [UserController::class, 'pdf_detail'])->name('pdf-master-barang-detail')->middleware('checkRole:Admin');

    // DATA Print Detail
    Route::get('/print-transaksi-pembelian-detail/{id}', [TransaksiPembelianController::class, 'print_detail'])->name('print-transaksi-pembelian-detail')->middleware('checkRole:Admin,Kasir');
    Route::get('/print-transaksi-pembelian-barang-detail/{id}', [TransaksiPembelianbarangController::class, 'print_detail'])->name('print-transaksi-pembelian-barang-detail')->middleware('checkRole:Admin,Kasir');
    Route::get('/print-master-barang-detail/{id}', [MasterbarangController::class, 'print_detail'])->name('print-master-barang-detail')->middleware('checkRole:Admin');
    Route::get('/print-user-detail/{id}', [UserController::class, 'print_detail'])->name('print-master-barang-detail')->middleware('checkRole:Admin');

    // DATA Print Excel
    Route::get('/pdf-transaksi-pembelian-detail/{id}', [TransaksiPembelianController::class, 'pdf_detail'])->name('pdf-transaksi-pembelian-detail')->middleware('checkRole:Admin,Kasir');
    Route::get('/pdf-transaksi-pembelian-barang-detail/{id}', [TransaksiPembelianbarangController::class, 'pdf_detail'])->name('pdf-transaksi-pembelian-barang-detail')->middleware('checkRole:Admin,Kasir');
    Route::get('/pdf-master-barang-detail/{id}', [MasterbarangController::class, 'pdf_detail'])->name('pdf-master-barang-detail')->middleware('checkRole:Admin');
    Route::get('/pdf-user-detail/{id}', [UserController::class, 'pdf_detail'])->name('pdf-master-barang-detail')->middleware('checkRole:Admin');

});







